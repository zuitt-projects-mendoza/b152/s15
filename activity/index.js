let user1 = {
	username: "jackscepticeye",
	email: "seanScepticeye@gmail.com",
	age: 31,
	topics: ["Gaming","Commentary"]
}

function isLegalAge() {
	if(user1.age >= 18){
		console.log("Welcome to the Club.");
	} else {
		console.log("You cannot enter the club yet.");
	}
}

isLegalAge(user1);

function addTopic(topic) {
	if(topic.length >= 5) {
		user1.topics.push(topic);
	} else {
		console.log("Enter a topic with at least 5 characters.");
	}
	
}

addTopic("Math");
addTopic("Comedy");
console.log(user1.topics);

let clubMembers = ["jackscepticeye","pewdiepie"]

function register(user) {
	if(user.length>=8) {
		clubMembers.push(user);
	} else {
		console.log("Please enter a suername longer or equal to 8 characters.")
	}
}

register("markiplier");
register("raven");
console.log(clubMembers);